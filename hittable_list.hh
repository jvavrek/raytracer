#ifndef HITTABLE_LIST_H
#define HITTABLE_LIST_H

#include "hittable.hh"

#include <vector>

#include "material.hh"
#include "sphere.hh"
#include "utils.hh"

class hittable_list : public hittable {
public:
    hittable_list() {};
    hittable_list(shared_ptr<hittable> object) { add(object); };

    void clear() {objects.clear();};
    void add(shared_ptr<hittable> object) {objects.push_back(object);};

    virtual bool hit(
        const ray& r, double t_min, double t_max, hit_record& rec
    ) const override;

    //virtual bool intersects(hittable *other) const override;

    //bool check_overlaps();

    std::vector<shared_ptr<hittable>> objects;
};

bool hittable_list::hit(const ray& r, double t_min, double t_max, hit_record& rec) const {
    hit_record temp_rec;
    bool hit_anything = false;
    auto closest_so_far = t_max;

    // ray may pass through multiple objects, so find closest to camera
    for (const auto& object : objects) {
        if (object->hit(r, t_min, closest_so_far, temp_rec)) {
            hit_anything = true;
            closest_so_far = temp_rec.t;
            rec = temp_rec;
        }
    }
    return hit_anything;
}

// bool hittable_list::check_overlaps() {
//     // Currently exact test whether any pair of volumes overlaps
//     for (size_t i = 0 ; i < objects.size(); ++i) {
//        for (size_t j = i + 1; j < objects.size(); ++j) {
//             std::cout << i << " " << j << std::endl;
//             auto oi = (sphere*) *(objects[i]);
//             auto oj = (sphere*) *(objects[j]);
//             if (oi.intersects(oj))
//                 return false;
//         }
//     }
//     return true;
// }

hittable_list my_scene() {
    hittable_list world;
    auto mat_ground = make_shared<lambertian>(color(0.4, 0.8, 0.1));
    auto mat_center = make_shared<lambertian>(color(0.9, 0.1, 0.1));
    auto mat_left   = make_shared<metal>(color(0.8, 0.8, 0.8), 0.3);
    auto mat_right  = make_shared<dielectric>(1.3);

    world.add(make_shared<sphere>(point3(0, -100.5, -1), 100, mat_ground));
    world.add(make_shared<sphere>(point3( 0, 0, -1), 0.5, mat_center));
    world.add(make_shared<sphere>(point3(-1, 0, -1), 0.5, mat_left));
    world.add(make_shared<sphere>(point3( 1, 0, -1), -0.5, mat_right));
    return world;
}

hittable_list random_scene() {
    hittable_list world;

    // Ground sphere
    auto ground_material = make_shared<lambertian>(color(0.5, 0.5, 0.5));
    world.add(make_shared<sphere>(point3(0, -1000, 0), 1000, ground_material));

    // Random small spheres on the ground
    for (int a = -11; a < 11; ++a) {
        for (int b = -11; b < 11; ++b) {
            point3 center(a + 0.9*random_double(), 0.2, b + 0.9*random_double());

            if ((center - point3(4, 0.2, 0)).length() < 0.9)
                continue;

            shared_ptr<material> sphere_material;
            auto choose_mat = random_double();
            if (choose_mat < 0.60) {
                // Diffuse
                auto albedo = color::random() * color::random();
                sphere_material = make_shared<lambertian>(albedo);
            } else if (choose_mat < 0.80) {
                // Metal
                auto albedo = color::random(0.5, 1);
                auto fuzz = random_double(0, 0.7);
                sphere_material = make_shared<metal>(albedo, fuzz);
            } else {
                // Glass
                auto ref_idx = random_double(1.2, 1.8);
                sphere_material = make_shared<dielectric>(ref_idx);
            }
            world.add(make_shared<sphere>(center, 0.2, sphere_material));
        }
    }

    // Big spheres
    auto material1 = make_shared<dielectric>(1.5);
    world.add(make_shared<sphere>(point3(0, 1, 0), 1.0, material1));

    auto material2 = make_shared<lambertian>(color(0.4, 0.2, 0.1));
    world.add(make_shared<sphere>(point3(-4, 1, 0), 1.0, material2));

    auto material3 = make_shared<metal>(color(0.7, 0.6, 0.5), 0.0);
    world.add(make_shared<sphere>(point3(4, 1, 0), 1.0, material3));

    return world;
}

#endif