# ppm_to_gif.py
# based on https://stackoverflow.com/questions/4270700

import re
import numpy as np
from PIL import Image

def ppm2pil(filename):
    """Read a .ppm file into a PIL object via numpy."""
    fin = None
    debug = True

    try:
        fin = open(filename, 'r')

        # Get file type
        while True:
            header = fin.readline().strip()
            if header.startswith('#'):
                continue
            elif header == 'P3':
                break
            else:
                # Unexpected header.
                if debug:
                    print('Bad mode:', header)
                return None

        # Get rows, cols
        rows, cols = 0, 0
        while True:
            header = fin.readline().strip()
            if header.startswith('#'):
                continue
        
            match = re.match('^(\d+) (\d+)$', header)
            if match == None:
                if debug:
                    print('Bad size:', repr(header))
                return None

            cols, rows = match.groups()
            break

        rows = int(rows)
        cols = int(cols)

        assert rows != 0
        assert cols != 0

        if debug:
            print('Rows: %d, cols: %d' % (rows, cols))
        
        # Get max_val
        max_val = np.int8(fin.readline().strip())

        # Initialise a 3D numpy array 
        result = np.zeros((rows, cols, 3), np.uint8)

        pxs = []

        # Read to EOF.
        while True:
            line = fin.readline().strip()
            if line == '':
                break

            line = [int(x) for x in line.split(' ')]
            pxs.append(line)

        if len(pxs) != rows*cols:
            if debug:
                print('Insufficient image data:', len(pxs))
            return None

        for r in range(rows):
            for c in range(cols):
                # Index into the numpy array and set the pixel value.
                result[r, c] = pxs[r*cols + c]

        return Image.fromarray(result)

    finally:
        if fin != None:
            fin.close()
        fin = None

    return None

im_list = [ppm2pil('output/img_{:d}.ppm'.format(i)) for i in range(50)]
im_list[0].save(
    'output/render.gif',
    save_all=True,
    append_images=im_list[1:],
    duration=100,
    loop=0
)
