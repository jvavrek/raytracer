#ifndef CAMERA_H
#define CAMERA_H

#include "utils.hh"

class camera {
public:
    camera(
        point3 lookfrom = point3(0, 0, 0),
        point3 lookat = point3(0, 0, -1),
        vec3 vup = vec3(0, 1, 0),
        double fov_vert = 90.,
        double aspect_ratio = 16./9.,
        double aperture = 0.1,
        double focus_dist = 1.0
    ) {
        auto theta = fov_vert * rad_per_deg;
        auto h = tan(0.5*theta);
        auto viewport_height = 2.0 * h;
        auto viewport_width = aspect_ratio * viewport_height;

        w = unit_vector(lookfrom - lookat);
        u = unit_vector(cross(vup, w));
        v = cross(w, u);

        origin = lookfrom;
        horizontal = focus_dist * viewport_width * u;
        vertical = focus_dist * viewport_height * v;
        lower_left = origin - horizontal/2. - vertical/2. - focus_dist*w;
        lens_radius = aperture * 0.5;
    }

    ray get_ray(double s, double t) const {
        vec3 rd = lens_radius * random_in_unit_disk();
        vec3 offset = u * rd.x() + v * rd.y();
        return ray(origin + offset, lower_left + s*horizontal + t*vertical - origin - offset);
    }

private:
    point3 origin;
    point3 lower_left;
    vec3 horizontal;
    vec3 vertical;
    vec3 u, v, w;
    double lens_radius;
};

#endif