#include <iostream>
#include <fstream>
#include <ctime>
#include <thread>
#include <algorithm>
#include <sstream>

#include "utils.hh"

#include "hittable_list.hh"
#include "sphere.hh"
#include "color.hh"
#include "camera.hh"
#include "material.hh"
#include "image.hh"

color ray_color(const ray& r, const hittable& world, int depth) {
    hit_record rec;

    // Limit the recursion depth by number of ray bounces
    if (depth <= 0) {
        return color(0, 0, 0);
    }

    if (world.hit(r, 0.001, inf, rec)) {
        ray scattered;
        color attenuation;
        // Recursively compute the next ray trace based on the material, losing
        // a fraction of the color on each bounce
        if (rec.mat_ptr->scatter(r, rec, attenuation, scattered)) {
            return attenuation * ray_color(scattered, world, depth-1);
        }
        return color(0, 0, 0);
    }

    // Otherwise, gradient background
    vec3 unit_direction = unit_vector(r.direction());
    auto t = 0.5*(unit_direction.y() + 1.0);
    return (1.0-t) * color(1., 1., 1.) + t * color(0.5, 0.7, 1.0);
}

image render(
    image &img,
    camera cam,
    hittable_list world,
    int img_height=400,
    int img_width=600,
    int samples_per_pixel=100,
    int max_depth=50,
    int seed=0
) {
    srand(seed);
    for (int j=img_height-1; j>=0; --j) {
        std::cout << "\rScanlines remaining: " << j << " / " << img_height
                  << std::flush;
        std::vector<color> row;
        for (int i=0; i<img_width; ++i) {
            color pixel_color(0, 0, 0);
            for (int s=0; s<samples_per_pixel; ++s) {
                auto u = (i + 0*random_double()) / (img_width-1);
                auto v = (j + 0*random_double()) / (img_height-1);
                ray r = cam.get_ray(u, v);
                pixel_color += ray_color(r, world, max_depth);
            }
            row.push_back(pixel_color);
        }
        img.push_back(row);
    }
    return img;
}

int main() {
    clock_t start = clock();
    // Image
    const double aspect_ratio = 3./2.;
    const int img_width = 300;
    const int img_height = static_cast<int> (img_width / aspect_ratio);
    const int samples_per_pixel = 15;
    const int max_depth = 50;

    // World
    //hittable_list world = my_scene();
    hittable_list world = random_scene();

    //world.check_overlaps();

    // Camera trajectory
    const int frames = 1;
    const double frames_per_rot = 50.;
    for (int frame = 0; frame < frames; ++frame) {
        std::cout << "\nframe " << frame << " / " << frames << std::endl;
        point3 lookfrom(
            13*sin(2*pi*frame/frames_per_rot),
            2-sin(2*pi*frame/frames_per_rot),
            3*cos(2*pi*frame/frames_per_rot)
        );
        point3 lookat(0,0,0);
        vec3 vup(0,1,0);
        auto dist_to_focus = 10.0;
        auto aperture = 0.1;
        camera cam(lookfrom, lookat, vup, 20, aspect_ratio, aperture, dist_to_focus);

        // Render!
        const int jobs = 3;
        image_list sub_images(jobs);
        std::vector<std::thread> threads;
        for (int job = 0; job < jobs; ++job) {
            std::cout << "\njob " << job << std::endl;
            image img;

            threads.push_back(std::thread(render, std::ref(sub_images[job]), cam, world, img_height, img_width, samples_per_pixel, max_depth, job));

            // NOTE for posterity: sequential method:
            // render(img, cam, world, img_height, img_width, samples_per_pixel, max_depth);
            // sub_images[job] = img;
        }
        for (auto &th : threads)
            th.join();
        std::cout << std::endl;
        std::cout << "Averaging over jobs" << std::endl;
        image final_img = sum_image(sub_images);  // TODO: parallelize!

        std::cout << sub_images[0][100][100] << std::endl;
        std::cout << sub_images[1][100][100] << std::endl;
        std::cout << sub_images[2][100][100] << std::endl;
        std::cout << sub_images[0][156][92] << std::endl;
        std::cout << sub_images[1][156][92] << std::endl;
        std::cout << sub_images[2][156][92] << std::endl;

        // Write!
        std::cout << "Writing to file" << std::endl;
        std::stringstream ofile_s("");
        ofile_s << "output/img_" << frame << ".ppm";
        std::string ofile_name = ofile_s.str();
        std::ofstream ofile;
        ofile.open(ofile_name);
        ofile << "P3\n" << img_width << ' ' << img_height << "\n255\n";  // header
        for (int j = 0; j < img_height; ++j) {
            for (int i = 0; i < img_width; ++i) {
                auto pixel_color = prepare_for_write(final_img[j][i], jobs*samples_per_pixel);
                //write_color(ofile, pixel_color);
            }
        }
        auto stop = clock();
        auto render_time = (double (stop - start)) / CLOCKS_PER_SEC;
        std::cout << std::endl;
        std::cout << "Created new file: " << ofile_name << std::endl;
        std::cout << "Render time: " << render_time << " s" << std::endl;
    }

    auto stop = clock();
    auto render_time = (double (stop - start)) / CLOCKS_PER_SEC;
    std::cout << std::endl;
    std::cout << "Total render time: " << render_time << " s" << std::endl;
    return 0;
}
