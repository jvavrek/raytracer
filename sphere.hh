#ifndef SPHERE_H
#define SPHERE_H

#include "hittable.hh"
#include "utils.hh"

class sphere : public hittable {
public:
    sphere() {};
    sphere(point3 cen, double r, shared_ptr<material> m)
        : center(cen), radius(r), mat_ptr(m) {};

    virtual bool hit(
        const ray& r, double t_min, double t_max, hit_record& rec
    ) const override;

    //virtual bool intersects(hittable *other) const override;

    point3 center;
    double radius;
    shared_ptr<material> mat_ptr;
};

bool sphere::hit(const ray& r, double t_min, double t_max, hit_record& rec) const {
    // vector from origin to sphere center
    vec3 oc = r.origin() - center;

    // set up modified quadratic formula
    auto a = r.direction().length_squared();
    auto half_b = dot(oc, r.direction());
    auto c = oc.length_squared() - radius*radius;
    auto disc = half_b*half_b - a*c;

    // no intersection
    if (disc < 0)
        return false;
    
    // one or more intersections
    auto sqrtd = sqrt(disc);
    auto root = (-half_b - sqrtd) / a;  // t-coord of intersection

    // check if root is in acceptable range; if not, try other solution
    if (root < t_min || t_max < root) {
        root = (-half_b + sqrtd) / a;
        // if still outside acceptable range, there is no acceptable hit
        if (root < t_min || t_max < root) {
            return false;
        }
    }

    // create the hit record
    rec.t = root;  // t-coord
    rec.p = r.at(rec.t);  // point
    vec3 outward_normal = (rec.p - center) / radius;  // surface normal
    rec.set_face_normal(r, outward_normal);  // surf norm w/ direction tracked
    rec.mat_ptr = mat_ptr;  // material
    return true;
}

// bool sphere::intersects(hittable *other) const {
//     return false;
// }

#endif