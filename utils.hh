#ifndef UTILS_H
#define UTILS_H

//
// Common utilities across the raytracer project
//

#include <cmath>
#include <limits>
#include <memory>
#include <cstdlib>

// Usings
using std::shared_ptr;
using std::make_shared;
using std::sqrt;
using std::sin;
using std::cos;

// Constants
const double inf = std::numeric_limits<double>::infinity();
const double pi = 3.1415926535897932385;
const double rad_per_deg = pi / 180.;

// Functions
inline double random_double() {
    // Random real in [0, 1)
    return rand() / (RAND_MAX + 1.0);
}

inline double random_double(double min, double max) {
    // Random real in [min, max)
    return min + (max - min) * random_double();
}

inline double clamp(double x, double min, double max) {
    if (x < min) return min;
    if (x > max) return max;
    return x;
}

// Common headers
#include "ray.hh"
#include "vec3.hh"

#endif