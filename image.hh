#ifndef IMAGE_H
#define IMAGE_H

#include <vector>

#include "color.hh"

class image;
typedef std::vector<image> image_list;

class image {
public:
    image() {};
    ~image() {};

    int rows() {return img.size();};
    int cols() {return img[0].size();};

    std::vector<color> operator[](int i) const {return img[i];}
    std::vector<color>& operator[](int i) {return img[i];}

    image& operator+=(const image &other) {
        for (int j = 0; j < rows(); ++j) {
            for (int i = 0; i < cols(); ++i) {
                img[j][i] += other[j][i];
            }
        }
        return *this;
    }

    image& operator/=(double x) {
        for (int j = 0; j < rows(); ++j) {
            for (int i = 0; i < cols(); ++i) {
                img[j][i] /= x;
            }
        }
        return *this;
    }

    void push_back(std::vector<color> row) {img.push_back(row);};

private:
    std::vector<std::vector<color>> img;
};

image sum_image(image_list images) {
    const int n = images.size();
    image s = images[0];
    for (int i = 1; i < n; ++i) {
        s += images[i];
    }
    return s;
}

#endif
