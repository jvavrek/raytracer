#ifndef HITTABLE_H
#define HITTABLE_H

#include "utils.hh"

class material;

// Simple record of info at a hit. Normals are always defined to point outside.
struct hit_record {
    point3 p;
    vec3 normal;
    shared_ptr<material> mat_ptr;
    double t;
    bool front_face;

    inline void set_face_normal(const ray& r, const vec3& outward_normal) {
        // if the dot product between the ray and the always-defined-outward
        // normal is negative, then they are anti-aligned: the ray started
        // outside the volume and struck the front face, not the interior face
        front_face = dot(r.direction(), outward_normal) < 0;
        normal = front_face ? outward_normal : -outward_normal;
    }
};

// Abstract hittable class, with virtual hit method
class hittable {
    public:
        virtual bool hit(const ray& r, double t_min, double t_max, hit_record &rec) const = 0;
        //virtual bool intersects(hittable *other) const = 0;
};

#endif