#ifndef COLOR_H
#define COLOR_H

#include "vec3.hh"

#include <iostream>
#include <fstream>

color prepare_for_write(color pixel_color, int samples_per_pixel) {
    auto r = pixel_color.x();
    auto g = pixel_color.y();
    auto b = pixel_color.z();

    // Divide by samples per pixel and correct for gamma=2.
    auto scale = 1.0 / samples_per_pixel;
    r = sqrt(r * scale);
    g = sqrt(g * scale);
    b = sqrt(b * scale);

    // Clamp values to [0, 0.999] and scale range to 256
    r = static_cast<int>(256*clamp(r, 0., 0.999));
    g = static_cast<int>(256*clamp(g, 0., 0.999));
    b = static_cast<int>(256*clamp(b, 0., 0.999));

    return color(r, g, b);
}

void write_color(std::ostream &ofile, color pixel_color) {
    ofile << pixel_color.x() << ' '
          << pixel_color.y() << ' '
          << pixel_color.z() << '\n';
}

#endif