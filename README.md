C++ raytracer based heavily on [_Ray Tracing in One Weekend_](https://raytracing.github.io/books/RayTracingInOneWeekend.html)

Custom additions:
 1. multiple frames with moving camera, output to animated gif
 2. parallelization over multiple CPUs
